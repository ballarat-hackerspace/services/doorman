const https = require('https')
const mqtt = require('mqtt')
const sqlite = require('sqlite')
const { IncomingWebhook } = require('@slack/client')

// latch values
const OPEN = 1
const CLOSED = 0

// how often to send reminders/warnings (ms)
const WARNING = process.env.OPEN_WARNING ? Number(process.env.OPEN_WARNING) : 15*60*1000
const REMINDER = process.env.OPEN_REMINDER ? Number(process.env.OPEN_REMINDER) : 60*60*1000

const slack_url = process.env.SLACK_WEBHOOK_URL
const webhook = new IncomingWebhook(slack_url)
const dbPromise = Promise.resolve()
  .then(() => sqlite.open('./db/doorman.sqlite', { Promise }))
  .then(db => db.migrate())

var options = {
  will: {
    topic: process.env.MQTT_PUBTOPIC,
    payload: 'offline',
  },
}
var client = mqtt.connect(process.env.MQTT_SERVER, options)
var latch = null
var last_state = Date.now()
var last_msg = Date.now()
var open_alert = false
var next_reminder = null

function mattermost_send(msg) {
  const data = JSON.stringify({
      text: msg
  })

  const options = {
    hostname: 'chat.ballarat.tech',
    port: 443,
    path: process.env.MATTERMOST_HOOK_PATH,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': data.length
    }
  }
  const req = https.request(options, res => {
    console.log(`statusCode: ${res.statusCode}`)

    res.on('data', d => {
      console.log(d)
    })
  })
  req.write(data)
  req.end()
}

function slack_send(msg) {
  webhook.send(msg, function(err, res) {
    if (err) {
      console.log('Slack Error:', err)
    } else {
      console.log('Slack Message sent: ', res)
    }
  })
}

// constant reminder until closed
function reminder() {
  slack_send(`Front door is still open`)
  mattermost_send(`Front door is still open`)
  next_reminder = setTimeout(reminder, REMINDER)
}

client.on('connect', function () {
  client.publish(process.env.MQTT_PUBTOPIC, 'online')
  client.subscribe(process.env.MQTT_TOPIC, function (err) {
    if (!err) {
      console.log("Listening for door latch status")
    }
  })
})

client.on('message', function (topic, message) {
  var latch_now = Number(message)
  last_msg = Date.now()

  // if state has changed

  if (latch != latch_now) {
    console.log(`Door state changed`)
    // record last state change
    last_state = Date.now()

    if (latch == null) {                            // First run
      console.log(`Initial door state received`)
      latch = latch_now
    }
    else if (latch_now == CLOSED) {                 // Door Closed
      open_alert = false

      console.log(`Front door has closed`)
      client.publish(process.env.MQTT_PUBTOPIC, 'closed')
      slack_send(`Front door has closed`)
      mattermost_send(`Front door has closed`)

      const dbPromise = Promise.resolve()
        .then(() => sqlite.open('./db/doorman.sqlite', { Promise }))
        .then(db => db.run(`INSERT INTO Events(name) VALUES('CLOSED')`))

      // cancel any running reminders
      if (next_reminder != null) {
        clearTimeout(next_reminder)
      }
    }
    else if (latch_now == OPEN) {                   // Door Open
      console.log(`Front door has been opened`)
      client.publish(process.env.MQTT_PUBTOPIC, 'opened')
      slack_send(`Front door has been opened`)
      mattermost_send(`Front door has been opened`)

      const dbPromise = Promise.resolve()
        .then(() => sqlite.open('./db/doorman.sqlite', { Promise }))
        .then(db => db.run(`INSERT INTO Events(name) VALUES('OPENED')`))
    }
  }

  var since_last_state_change = Date.now() - last_state
  console.log(`Latch: ${latch_now}, Last State Change: ${(since_last_state_change/(1000*60)).toFixed(2)}mins ago`)

  // if held open for more than 15min send an alert

  if (open_alert == false && latch_now == OPEN && since_last_state_change > WARNING) {
    var time_since = Math.floor(since_last_state_change/(1000*60))
    console.log(`Front door has been open for about ${time_since} minutes`)
    slack_send(`Front door has been open for about ${time_since} minutes`)
    mattermost_send(`Front door has been open for about ${time_since} minutes`)

    const dbPromise = Promise.resolve()
      .then(() => sqlite.open('./db/doorman.sqlite', { Promise }))
      .then(db => db.run(`INSERT INTO Events(name) VALUES('HELD_OPEN')`))

    if (next_reminder == null) {
      next_reminder = setTimeout(reminder, REMINDER)
    }

    open_alert = true
  }

  latch = latch_now
})

console.log(`Will warn of door open longer than ..... : ${WARNING/(1000*60)}min`)
console.log(`Then will remind of door open every .... : ${REMINDER/(1000*60)}min`)

