# Doorman

A small nodejs based door monitor that alerts to slack

# Testing locally

```
$ cp prod.env-template .env
$ vi .env
$ npm run dev
```

# Installing on server

```
$ sudo -s
# cd /opt
# git clone https://gitlab.com/ballarat-hackerspace/services/doorman.git
# cd doorman
# npm run build-docker
```

# Run in Production

## First time configuration

Use the env template file and set your variables (slack webhook url, mqqt details
etc):

```
# cp prod.env-template prod.env
# vi prod.env
```

## Run the service

```
# npm run service
```

# Stopping service

```
# npm run kill
```

# Checking the logs

```
# npm run logs
```
