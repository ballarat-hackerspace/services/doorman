FROM node:11-alpine

ADD index.js package.json /app/
ADD migrations /app/migrations/

WORKDIR /app

RUN set -x \
  && npm run build

ENTRYPOINT ["npm"]
CMD ["run", "start"]
