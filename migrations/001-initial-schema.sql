-- Up
CREATE TABLE Events (dt DATETIME DEFAULT current_timestamp PRIMARY KEY, name TEXT);
INSERT INTO Events (name) VALUES ('Initialised');

-- Down
DROP TABLE Events;
